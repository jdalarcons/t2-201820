package model.data_structures;

public class Node <T extends Comparable<T>> 
{
	public T item;
	public Node<T> next;
	public Node<T> previous;
	
	public Node(T item)
	{
		this.item = item;
	}
	
	public void setNext(Node<T> next)
	{
		this.next = next;
	}
	
	public void setPrevious(Node<T> previous)
	{
		this.previous = previous;
	}
	

}
