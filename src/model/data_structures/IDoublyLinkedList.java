package model.data_structures;

import java.util.ListIterator;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T extends Comparable<T>> extends Iterable<T> 
{
	public void add( T item);
	public void delete ( T item);
	public int size();
	public T get (int i);
	public ListIterator<T> iterator();
	
	
	



}
